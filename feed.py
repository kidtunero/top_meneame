# -*- coding: utf-8 -*-

import argparse
import logging
import logging.config
import time

import requests
import urllib3
import os

from pymongo import MongoClient

urllib3.disable_warnings()

parser = argparse.ArgumentParser(
    description='Registra actividad de meneame.net')

parser.add_argument(
    '-d', '--debug',
    help="Trazas a cascoporro",
    action="store_const", dest="loglevel", const=logging.DEBUG,
    default=logging.WARNING,
)
parser.add_argument(
    '-v', '--verbose',
    help="Algunas trazas",
    action="store_const", dest="loglevel", const=logging.INFO,
)
arg = parser.parse_args()

logging.config.fileConfig('logging.ini')
logger = logging.getLogger()
logger.setLevel(arg.loglevel)
'''
if arg.loglevel == logging.DEBUG:
    import http.client as http_client
    http_client.HTTPConnection.debuglevel = 1
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True
'''
sneaker = "https://www.meneame.net/backend/sneaker2?items=9999999&nopublished=1&time="
client = MongoClient('127.0.0.1:27017')
db = client.Meneame

t = "1"
max_slp = 60 * 10
crt_slp = (60 * 2) + 10


def get_events():
    global t
    global c
    url = sneaker + t
    c = time.time()
    r = requests.get(url, verify=False)
    if r.status_code not in (200, 202) or len(r.text) == 0:
        msg = "%d %s\n\t%s" % (r.status_code, url, r.text)
        logger.error(msg.strip())
        return []
    js = r.json()
    t = js["ts"]
    return js["events"]

while True:
    events = get_events()
    count = len(events)
    rate = count / crt_slp
    logger.info("%d eventos en %d segundos (%0.1f e/s)" %
                (count, crt_slp, rate))

    for e in events:
        if int(e["uid"]) > 0:
            db.events.insert_one(e)
            # db.events.update(e, e, {upsert:true})

    if count > 90:
        crt_slp = crt_slp - 2
    elif count < 70 and crt_slp < max_slp:
        crt_slp = crt_slp + 2

    logger.info("Durmiendo %d segundos" % (crt_slp,))

    slp = int(crt_slp - (time.time() - c))
    if slp > 0:
        time.sleep(slp)

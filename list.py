from pymongo import MongoClient
from bson.code import Code
import requests
import re
import sys

u = re.compile(r'.* href="/user/([^"]+)".*')

client = MongoClient('localhost:27017')
db = client.Meneame

'''
c=db.events.find().count()
print (str(c))
#sys.exit()
'''

nicks={}

js = db.events.group(
    key={ "who": 1, 'uid': 1 },
    condition={ "type": { "$ne": "problem" } },
    reduce=Code("function ( curr, result ) { }"),
    initial={ }
)

for j in js:
    nicks[j["uid"]] = j["who"]

def get(id):
    if id not in nicks:
        r = requests.get("https://www.meneame.net/geo/author.php?id="+str(id), verify=False)
        nicks[id]=u.findall(r.text)[0]
    return nicks[id]

def js_print(js):
    for j in js:
        print ("%3d %s" % (j["count"], get(j["_id"])))
    print ("")


js = db.events.aggregate([
    {"$group" : {"_id":"$uid", "count":{"$sum":1}}},
    {"$sort": {'count':-1}},
    {"$limit": 10},
])

print ("====== TOP =========")
js_print(js)

types = db.events.aggregate([
    {"$group" : {"_id":"$type"}}
])

for t in types:
    t = t["_id"]
    print ("====== "+t+" =========")
    js = db.events.aggregate([
        {"$match": {"type": t}},
        {"$group" : {"_id":"$uid", "count":{"$sum":1}}},
        {"$sort": {'count':-1}},
        {"$limit": 10},
    ])
    js_print(js)
